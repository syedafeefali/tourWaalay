<?php

namespace App\Http\Controllers;

use App\Models\client;
use App\Models\tour;
use Illuminate\Http\Request;

class mainController extends Controller
{

    public function index(){
        return view('frontend.view');
    }
    public function tour(){
        $tours = tour::all();
        return view('frontend.tour',compact('tours'));
    }
    public function  tourDetail(tour $id){
        $tour = $id;
        return view('frontend.tourDetail',compact('tour'));
    }
    public function about(){
        return view('frontend.about');
    }
    public function contact(){
        return view('frontend.contact');
    }
    public function clientAdd(Request $request)
    {
        $input = $request->all();
        unset($input['_token']);
        $parent = '';
        $counter = count($input["name"]);
        // return $input;
        for($i=0;$i<$counter;$i++){
          if($i == 0){
            $parent =  client::create([ 'name' => $input["name"][$i] ,
             'phone' => $input["phone"][$i] ,
             'email' => $input["email"][$i] ,
             'tour_id' => $request->tour_id,
             'cnic' => $input["cnic"][$i] ]);
          }else{
             client::create([ 'name' => $input["name"][$i] ,
            'phone' => isset($input["phone"][$i]) ? $input["phone"][$i] : ''  ,
            'email' => isset($input["email"][$i]) ? $input["email"][$i] : ''  ,
            'tour_id' => $request->tour_id,
            'cnic' => isset($input["cnic"][$i]) ? $input["cnic"][$i] : '' ,
            'parent_id'=>$parent->id 
            
            ]);
          }
            
        }
        return redirect()->back()->with('success','Thanks for booking, We will contact you soon.');

    }

}
