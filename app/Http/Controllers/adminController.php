<?php

namespace App\Http\Controllers;

use App\Models\client;
use App\Models\tour;
use Auth;
use Hash;
use Illuminate\Http\Request;

class adminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('backend.dashboard');
    }
    public function tourCreate(){
        return view('backend.tourCreate');
    }
    public function tourView(){
        $records = tour::all();
        return view('backend.tourView',compact('records'));
    }
    public  function tourInView(client $id){
         $data= $id;
        return view('backend.tourInView',compact('data'));
    }
    public  function  tourEdit(tour $id){
        $data = $id;
        return view('backend.tourEdit',compact('data'));
    }
    public  function  tourClone(tour $id){
        $data=$id;
        return view('backend.tourClone',compact('data'));
    }
    public function   tourSave(Request $request){
        // dd($request);
        $exclude = implode('|',$request->exclude);
        $include = implode('|',$request->include);
        
        if($request->hasfile('photo'))
                {
                    // $file = 'public/images/'.$post->image;
                    // if(!empty($post->image)){
                    //     if(File::exists($file)){
                    //         unlink($file);
                    //     }
                    // }

                    $tmp = $request->file('photo');
                    $image = time()."-".$tmp->getClientOriginalName();
                    $dest = 'frontend/assets/img/';
                    $tmp->move($dest,$image);
                    // $request->merge(['photo'=>$image]);
                }
                else{
                    $image = 'breadcrumb_bg2.jpg';
                }
                $request->merge([
                    'include'=>$include,
                    'exclude'=>$exclude
                ]);
        $tour = tour::create($request->all());
        $tour->photo = $image;
        $tour->update();
         return redirect()->route('admin.tourView')->with('success','Tour created');
    }
    public function tourUpdate(Request $request)
    {
        $tour = tour::find($request->id);
        $exclude = implode('|',$request->exclude);
        $include = implode('|',$request->include);
        $request->merge([
            'include'=>$include,
            'exclude'=>$exclude
        ]);
        if($request->hasfile('photo'))
        {
            // $file = 'public/images/'.$post->image;
            // if(!empty($post->image)){
            //     if(File::exists($file)){
            //         unlink($file);
            //     }
            // }

            $tmp = $request->file('photo');
            $image = time()."-".$tmp->getClientOriginalName();
            $dest = 'frontend/assets/img/';
            $tmp->move($dest,$image);
            // $request->merge(['photo'=>$image]);
        }
        else{
            $image = $tour->photo;
        }
        $tour->update($request->all());
        $tour->photo = $image;
        $tour->update();
        return redirect()->route('admin.tourView')->with('success','Tour updated');
    }
    public function tourClients(tour $id)
    {
        // return $id->clients;
        $clients = $id->clients;//->where('parent_id','==','');
        return view('backend.tourClient',compact('clients','id'));
    }
    public function tourDelete($id){
       
    }
    public  function memberView(){
        $clients = client::all();
        $id = tour::all();
        return view('backend.memberView',compact('clients','id'));
    }    
    public function clientEdit(client $id)
    {
        $client = $id;
        $clients = client::where('parent_id','=',NULL)->get();
        return view('backend.memberEdit',compact('client','clients'));
    }
    public function clientSave(Request $request)
    {
        client::create($request->all());
        return redirect()->back();
    }
    public function clientUpdate(Request $request)
    {
        $client =client::find($request->id);
        $client->update($request->all());
        $id = tour::all();
        return redirect()->route('admin.memberView',compact('id'));
    }
    public function profile(Request $request)
    {
        $user = Auth::user();
        $request->merge(['password'=> Hash::make($request->password)]);
        $user->update($request->all());
        return redirect()->back();
    }
}
