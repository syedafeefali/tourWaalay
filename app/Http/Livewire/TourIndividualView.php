<?php

namespace App\Http\Livewire;

use Livewire\Component;

class TourIndividualView extends Component
{
    public $client;
    public $name="Afeef";
    public function mount($client)
    {
       $this->client = $client;
    }
    public function render()
    {
        return view('livewire.tour-individual-view');
    }
}
