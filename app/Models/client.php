<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class client extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'phone','email','cnic','parent_id','tour_id','payment','payementStatus','tourStatus','count'
    ];
    public function persons()
    {
        return $this->hasMany(client::class,'parent_id','id');
    }
    public function tour()
    {
        return $this->belongsTo(tour::class,'tour_id','id');
    }
    public function parent()
    {
        return $this->belongsTo(client::class,'parent_id','id');
    }
}
