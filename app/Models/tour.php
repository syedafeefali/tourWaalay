<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tour extends Model
{
    use HasFactory;
    protected $fillable = [
        'title', 'days', 'startDate', 'endDate', 'status', 'price', 'photo', 'description','include','exclude','departure','destination','departureTime'
    ];
    public function clients()
    {
        return $this->hasMany(client::class,'tour_id','id');
    }
    

}
