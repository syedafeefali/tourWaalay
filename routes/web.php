<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\mainController;
use App\Http\Controllers\adminController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//  FrontEnd
Route::get('/home',[mainController::class,'index'])->name('home');
Route::get('/tour',[mainController::class,'tour'])->name('tour');
Route::get('/tour-detail/{id}',[mainController::class,'tourDetail'])->name('tourDetail');
Route::get('/about',[mainController::class,'about'])->name('about');
Route::get('/contact',[mainController::class,'contact'])->name('contact');
Route::post('/client-add',[mainController::class,'clientAdd'])->name('clientAdd');
//  Backend
Route::group(['prefix'=>'admin','as'=>'admin.'] ,function() {
    Route::get('/dashboard',[adminController::class,'index'])->name('dashboard');
    Route::get('/tour-create',[adminController::class,'tourCreate'])->name('tourCreate');
    Route::post('/tour-save',[adminController::class,'tourSave'])->name('tourSave');
    Route::get('/tour-edit/{id}',[adminController::class,'tourEdit'])->name('tourEdit');
    Route::post('/tour-update',[adminController::class,'tourUpdate'])->name('tourUpdate');
    Route::get('/tour-clone/{id}',[adminController::class,'tourClone'])->name('tourClone');
    Route::get('/tour-view',[adminController::class,'tourView'])->name('tourView');
    Route::get('/tour-clients/{id}',[adminController::class,'tourClients'])->name('tourClients');
    Route::get('/tour-individual-view/{id}',[adminController::class,'tourInView'])->name('tourInView');
    Route::get('/member-view',[adminController::class,'memberView'])->name('memberView');
    Route::get('/member-edit/{id}',[adminController::class,'clientEdit'])->name('memberEdit');
    Route::post('/member-update',[adminController::class,'clientUpdate'])->name('memberUpdate');
    Route::post('/member-save',[adminController::class,'clientSave'])->name('memberSave');
    Route::post('/profile',[adminController::class,'profile'])->name('profile');
    Route::get('profile', function () {
        return view('backend.profile');
    })->name('profile');
});
Route::get('logout',function(){
    Auth::logout(); 
    return redirect('/home');
});
Route::get('/',function(){
    return redirect('/home');
});
Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
