<div class="row">
    <div class="col-md-4 d-flex">
        <div class="card profile-box flex-fill">
            <div class="card-body">
                <h3 class="card-title">Customer Information</h3>
                <ul class="personal-info">
                    <li>
                        <div class="title">Full Name.</div>
                        <div class="text" contenteditable="true">98765-4321045-5</div>
                        
                    </li>
                    <li>
                        <div class="title">CNIC.</div>
                        <div class="text" contenteditable="true">98765-4321045-5</div>
                    </li>
                    <li>
                        <div class="title">Tel</div>
                        <div class="text"   contenteditable="true" wire:model="name">{{ $name}}<a href="" wire:model="name">{{ $name}}</a></div>
                    </li>
                    <li>
                        <div class="title">Email</div>
                        <div class="text" contenteditable="true">email@gmail.com</div>
                    </li>
                    <li>
                        <div class="title">Friends</div>
                        <div class="text" contenteditable="true">3</div>
                    </li>
                    <li>
                        <div class="title">Date</div>
                        <div class="text" contenteditable="true">02-02-2021 05-02-2021</div>
                    </li>
                    <li>
                        <div class="title">Payment Status</div>
                        <div class="text" contenteditable="true">RS 5000 / 12000</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-4 d-flex">
        <div class="card profile-box flex-fill">
            <div class="card-body">
                <h3 class="card-title">Customer Information</h3>
                <ul class="personal-info">
                    <li>
                        <div class="title">Full Name.</div>
                        <div class="text" contenteditable="true">Sayed Afeef Ali Shah</div>
                    </li>
                    <li>
                        <div class="title">CNIC.</div>
                        <div class="text" contenteditable="true">98765-4321045-5</div>
                    </li>
                    <li>
                        <div class="title">Tel</div>
                        <div class="text" contenteditable="true"><a href="">9876543210</a></div>
                    </li>
                    <li>
                        <div class="title">Email</div>
                        <div class="text" contenteditable="true">email@gmail.com</div>
                    </li>
                    <li>
                        <div class="title">Friends</div>
                        <div class="text" contenteditable="true">3</div>
                    </li>
                    <li>
                        <div class="title">Date</div>
                        <div class="text" contenteditable="true">02-02-2021 05-02-2021</div>
                    </li>
                    <li>
                        <div class="title">Payment Status</div>
                        <div class="text" contenteditable="true">RS 5000 / 12000</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-4 d-flex">
        <div class="card profile-box flex-fill">
            <div class="card-body">
                <h3 class="card-title">Customer Information</h3>
                <ul class="personal-info">
                    <li>
                        <div class="title">Full Name.</div>
                        <div class="text" contenteditable="true">Sayed Afeef Ali Shah</div>
                    </li>
                    <li>
                        <div class="title">CNIC.</div>
                        <div class="text" contenteditable="true">98765-4321045-5</div>
                    </li>
                    <li>
                        <div class="title">Tel</div>
                        <div class="text" contenteditable="true"><a href="">9876543210</a></div>
                    </li>
                    <li>
                        <div class="title">Email</div>
                        <div class="text" contenteditable="true">email@gmail.com</div>
                    </li>
                    <li>
                        <div class="title">Friends</div>
                        <div class="text" contenteditable="true">3</div>
                    </li>
                    <li>
                        <div class="title">Date</div>
                        <div class="text" contenteditable="true">02-02-2021 05-02-2021</div>
                    </li>
                    <li>
                        <div class="title">Payment Status</div>
                        <div class="text" contenteditable="true">RS 5000 / 12000</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-4 d-flex">
        <div class="card profile-box flex-fill">
            <div class="card-body">
                <h3 class="card-title">Customer Information</h3>
                <ul class="personal-info">
                    <li>
                        <div class="title">Full Name.</div>
                        <div class="text" contenteditable="true">Sayed Afeef Ali Shah</div>
                    </li>
                    <li>
                        <div class="title">CNIC.</div>
                        <div class="text" contenteditable="true">98765-4321045-5</div>
                    </li>
                    <li>
                        <div class="title">Tel</div>
                        <div class="text" contenteditable="true"><a href="">9876543210</a></div>
                    </li>
                    <li>
                        <div class="title">Email</div>
                        <div class="text" contenteditable="true">email@gmail.com</div>
                    </li>
                    <li>
                        <div class="title">Friends</div>
                        <div class="text" contenteditable="true">3</div>
                    </li>
                    <li>
                        <div class="title">Date</div>
                        <div class="text" contenteditable="true">02-02-2021 05-02-2021</div>
                    </li>
                    <li>
                        <div class="title">Payment Status</div>
                        <div class="text" contenteditable="true">RS 5000 / 12000</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
