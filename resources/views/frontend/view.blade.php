@extends('main')
@section('content')

    <!-- Slider Area Start -->
    <section class="peulis-slider-area overlay">
        <div class="peulis-slide owl-carousel">
            <div class="slider-container slider-1">
                <div class="single-slider zoom"></div>
            </div>
            <div class="slider-container slider-2">
                <div class="single-slider zoom"></div>
            </div>
        </div>
        <div class="banner-area">
            <div class="banner-caption">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 content-home">
                            <div class="banner-welcome">
                                <h4 class="text">travel with us</h4>
                                <div class="caption-inner">
                                    <div class="ah-headline">
                                        <span class="typed-static">Enjoy</span>
                                        <span class="ah-words-wrapper">
                                            <b class="is-visible"> Adventure</b>
                                            <b> Holiday</b>
                                            <b> Mountain</b>
                                        </span>
                                    </div>
                                </div>
                                <a href="{{ route('contact') }}" class="book-now">BOOK NOW!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Slider Area End -->


    <!-- Promo Packages Area Start -->
    <section class="peulis-promo-package-area section_70">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="site-heading">
                        <h2>Hot Packages</h2>
                        <p>Lorem ipsum dolor sit amet, conseetuer adipiscing elit. Aenan comdo igula eget. Aenean massa
                            cum sociis Theme natoque.</p>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-4 pad-right">
                    <div class="single-promo-package">
                        <div class="promo-package-img">
                            <a href="#">
                                <img src="{{asset('frontend')}}/assets/img/promo-pack1.jpg" alt="Promo Packages" />
                            </a>
                        </div>
                        <div class="promo-package-desc">
                            <div class="package-desc-title">
                                <h3><a href="#">Shanghai</a></h3>
                            </div>
                            <div class="promo-pack-inner">
                                <div class="package-desc-meta">
                                    <p><span>3</span> Days</p>

                                </div>
                                <div class="package-desc-price">
                                    <p><span>Rs. 12,499</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-5 pad-left">
                            <div class="single-promo-package">
                                <div class="promo-package-img">
                                    <a href="#">
                                        <img src="{{asset('frontend')}}/assets/img/promo-pack2.jpg" alt="Promo Packages" />
                                    </a>
                                </div>
                                <div class="promo-package-desc">
                                    <div class="package-desc-title">
                                        <h3><a href="#">venice - Italy</a></h3>
                                    </div>
                                    <div class="promo-pack-inner">
                                        <div class="package-desc-meta">
                                            <p><span>3</span> Days</p>

                                        </div>
                                        <div class="package-desc-price">
                                            <p><span>Rs. 12,499</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="single-promo-package">
                                <div class="promo-package-img">
                                    <a href="#">
                                        <img src="{{asset('frontend')}}/assets/img/promo-pack3.jpg" alt="Promo Packages" />
                                    </a>
                                </div>
                                <div class="promo-package-desc">
                                    <div class="package-desc-title">
                                        <h3><a href="#">Portugal</a></h3>
                                    </div>
                                    <div class="promo-pack-inner">
                                        <div class="package-desc-meta">
                                            <p><span>3</span> Days</p>

                                        </div>
                                        <div class="package-desc-price">
                                            <p> <span>Rs. 12,499</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-7 pad-left">
                            <div class="single-promo-package bottom_pack">
                                <div class="promo-package-img">
                                    <a href="#">
                                        <img src="{{asset('frontend')}}/assets/img/promo-pack-4.jpg" alt="Promo Packages" />
                                    </a>
                                </div>
                                <div class="promo-package-desc">
                                    <div class="package-desc-title">
                                        <h3><a href="#">Maldives</a></h3>
                                    </div>
                                    <div class="promo-pack-inner">
                                        <div class="package-desc-meta">
                                            <p><span>3</span> Days</p>

                                        </div>
                                        <div class="package-desc-price">
                                            <p><span>Rs. 12,499</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="single-promo-package bottom_pack">
                                <div class="promo-package-img">
                                    <a href="#">
                                        <img src="{{asset('frontend')}}/assets/img/promo-pack5.jpg" alt="Promo Packages" />
                                    </a>
                                </div>
                                <div class="promo-package-desc">
                                    <div class="package-desc-title">
                                        <h3><a href="#">Amsterdam</a></h3>
                                    </div>
                                    <div class="promo-pack-inner">
                                        <div class="package-desc-meta">
                                            <p><span>3</span> Days</p>

                                        </div>
                                        <div class="package-desc-price">
                                            <p> <span>Rs. 12,499</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 text-right">
                    <a href="{{ route('tour') }}" class="view-all mt-3">View All</a>
                </div>
            </div>
        </div>
    </section>
    <!-- Promo Packages Area End -->

    <!-- Our Services Area Start -->
    <section class="peulis-choose-area section_70">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="site-heading">
                        <h2>Our Services</h2>
                        <p>We Offer The Following Services</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="our-service">
                        <div class="service-image">
                            <i class="fa fa-plane fa-3x"></i>
                        </div>
                        <div class="choose-desc">
                            <h3>Air Ticketing</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis molestiae vitae
                                eligendi at.</p>

                        </div>

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="our-service">
                        <div class="service-image">
                            <i class="fa fa-plane fa-3x"></i>
                        </div>
                        <div class="choose-desc">
                            <h3>Air Ticketing</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis molestiae vitae
                                eligendi at.</p>

                        </div>

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="our-service">
                        <div class="service-image">
                            <i class="fa fa-plane fa-3x"></i>
                        </div>
                        <div class="choose-desc">
                            <h3>Air Ticketing</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis molestiae vitae
                                eligendi at.</p>

                        </div>

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="our-service">
                        <div class="service-image">
                            <i class="fa fa-plane fa-3x"></i>
                        </div>
                        <div class="choose-desc">
                            <h3>Air Ticketing</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis molestiae vitae
                                eligendi at.</p>

                        </div>

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="our-service">
                        <div class="service-image">
                            <i class="fa fa-plane fa-3x"></i>
                        </div>
                        <div class="choose-desc">
                            <h3>Air Ticketing</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis molestiae vitae
                                eligendi at.</p>

                        </div>

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="our-service">
                        <div class="service-image">
                            <i class="fa fa-plane fa-3x"></i>
                        </div>
                        <div class="choose-desc">
                            <h3>Air Ticketing</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis molestiae vitae
                                eligendi at.</p>

                        </div>

                    </div>
                </div>


            </div>
        </div>
    </section>
    <!-- Our Services Area End -->

    <!-- Choose Area Start -->
    <section class="peulis-choose-area section_70">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="site-heading">
                        <h2>Why Peulis?</h2>
                        <p>Lorem ipsum dolor sit amet, conseetuer adipiscing elit. Aenan comdo igula eget. Aenean massa
                            cum sociis Theme natoque.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="single-choose">
                        <p>01</p>
                        <div class="choose-image">
                            <img src="{{asset('frontend')}}/assets/img/choose-1.png" alt="Safe Travel" />
                        </div>
                        <div class="choose-desc">
                            <h3>Safe Travel</h3>
                            <p>printing and typesetting industry has been printing the industry’s ipsum</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-choose">
                        <p>02</p>
                        <div class="choose-image">
                            <img src="{{asset('frontend')}}/assets/img/choose-2.png" alt="Awesome Guide" />
                        </div>
                        <div class="choose-desc">
                            <h3>Awesome Guide</h3>
                            <p>printing and typesetting industry has been printing the industry’s ipsum</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-choose">
                        <p>03</p>
                        <div class="choose-image">
                            <img src="{{asset('frontend')}}/assets/img/choose-3.png" alt="Save Money" />
                        </div>
                        <div class="choose-desc">
                            <h3>Save Money</h3>
                            <p>printing and typesetting industry has been printing the industry’s ipsum</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Choose Area End -->

    <!-- Travel with us Area Start -->
    <section class="peulis-choose-area section_70 border-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="site-heading">
                        <h2>Want To Travel With Us?</h2>
                        <p>
                            <a href="{{ route('contact') }}" class="book-now mt-3">BOOK NOW!</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Travel with us Area End -->

@endsection