    <!--Preloader Start-->
    <div class="preloader">
        <div class="loading-text">
            <span class="loading-text-words" data-preloader="L">L</span>
            <span class="loading-text-words" data-preloader="O">O</span>
            <span class="loading-text-words" data-preloader="A">A</span>
            <span class="loading-text-words" data-preloader="D">D</span>
            <span class="loading-text-words" data-preloader="I">I</span>
            <span class="loading-text-words" data-preloader="N">N</span>
            <span class="loading-text-words" data-preloader="G">G</span>
        </div>
    </div>
    <!-- Preloader End -->

<!-- Header Area Start -->
    <header class="peulis-header-area">

        <nav class="mobile-bottom-nav">
            <div class="mobile-bottom-nav__item">
                <a href="{{ route('home') }}">
                    <div class="mobile-bottom-nav__item-content">
                        <i class="fas fa-home fa-lg"></i>
                        <span> HOME</span>
                    </div>
                </a>
            </div>
            <div class="mobile-bottom-nav__item">
                <a href="{{ route('tour') }}">
                    <div class="mobile-bottom-nav__item-content">
                        <i class="fas fa-mountain fa-lg"></i>
                        <span>TOURS</span>
                    </div>
                </a>
            </div>
            <div class="mobile-bottom-nav__item">
                <a href="{{ route('contact') }}">
                    <div class="mobile-bottom-nav__item-content">
                        <i class="fas fa-envelope fa-lg"></i>
                        <span>CONTACT</span>
                    </div>
                </a>
            </div>
            <div class="mobile-bottom-nav__item ">
                <a href="{{ route('about') }}">
                    <div class="mobile-bottom-nav__item-content">
                        <i class="far fa-address-card fa-lg"></i>
                        <span>ABOUT</span>
                    </div>
                </a>
            </div>

        </nav>
        <!-- Header Top Area Start -->
        <div class="header-top-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="site-logo">
                            <a href="index.html">
                                <img src="{{asset('frontend')}}/assets/img/logo.png" alt="Peulis" />
                            </a>
                        </div>
                        <!-- Responsive Menu Start -->
                        <div class="peulis-responsive-menu"></div>
                        <!-- Responsive Menu End -->
                    </div>
                    <div class="col-lg-7">
                        <div class="mainmenu">
                            <nav>
                                <ul id="navigation_menu">
                                    <li class=""><a href="{{ route('home') }}">Home</a></li>
                                    <li class=""><a href="{{ route('tour') }}">Tours</a></li>
                                    <li class=""><a href="{{ route('contact') }}">Contact</a></li>
                                    <li class=""><a href="{{ route('about') }}">About</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="header_action">
                            <ul>
                                <li><a href="https://www.facebook.com/tripwaalay"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li><a href="https://www.instagram.com/tripwaalay/"><i class="fab fa-instagram"></i></a>
                                </li>
                                <li><a href="#" id="sidenav-toggle"><i class="fa fa-bars"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header Top Area End -->

        <!-- Main Header Area Start -->

        <!-- Main Header Area End -->
    </header>
    <!-- Header Area End -->
