
    <!-- Footer Area Start -->
    <footer class="peulis-footer-area">
        <div class="footer-top-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-sm-12 col-md-12">
                        <div class="single-footer">
                            <a href="tel:+0306-2202059" style="color: #16d17c;">Call US at 0306-2202059</a>
                            <h3 class="mt-2">Are you looking to
                                plan a trip of a lifetime?</h3>
                            <div class="footer-logo">
                                <a href="{{ route('home') }}">
                                    <img src="{{asset('frontend')}}/assets/img/logo.png" alt="Peulis" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="single-footer">
                            <h3>About Us</h3>
                            <ul>
                                <li><a href="#">Why Us</a></li>
                                <li><a href="#">Why Touring</a></li>
                                <li><a href="#">Reviews</a></li>
                                <li><a href="#">Travel Insurance</a></li>
                                <li><a href="#">Programme</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col">
                        <div class="single-footer">
                            <h3>Support</h3>
                            <ul>
                                <li><a href="#">Account</a></li>
                                <li><a href="#">Client Area</a></li>
                                <li><a href="#">Privacy & Policy</a></li>
                                <li><a href="#">Author Hangout</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="single-footer">
                            <h3>Pay Safely</h3>
                            <p>Eiusmod tempor incididunt utbor etian dolm magna aliqua enim minim</p>
                            <div class="payment_image">
                                <img src="{{asset('frontend')}}/assets/img/creditcard-logo.png" alt="Payment Card" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer-bottom-box">
                            <div class="row">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="footer-bottom-left">
                                        <p>&copy; Copyright Tripwaalay - By <a href="http://bzbeetech.com/"
                                                class="company">Busy Bee Technologies</a></p>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="footer-bottom-left">
                                        <ul>
                                            <li><a href="https://www.facebook.com/tripwaalay"><i
                                                        class="fab fa-facebook-f"></i></a></li>
                                            <li><a href="https://www.instagram.com/tripwaalay/"><i
                                                        class="fab fa-instagram"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Area End -->


    <!-- Sidebar Navigation Start -->
    <div class="ct-sidenav">
        <button class="close"><i class="fa fa-times"></i></button>
        <img src="{{asset('frontend')}}/assets/img/logo.png" alt="Site Logo" />
        <div class="sidenav-info">
            <p>Lorem Ipsum is simply dummied text of the printing and typesetting industry. Lorem Ipsum has been the
                industry's standard dummy text ever since when an unknown printer took a galley of type and scrambled it
                to make a type specimen book.printing and typesetting industry It has survived not only five centuries.
            </p>
            <ul class="sidenav-contact-info">
                <li>
                    <i class="fa fa-map-marker"></i>
                    <p>Lahore, Pakistan <span>Street # 1, Super Town</span></p>
                </li>
                <li>
                    <i class="fa fa-phone"></i>
                    <p> 0306-2202059 <span>Give us a call</span></p>
                </li>
                <li>
                    <i class="fa fa-envelope"></i>
                    <p> booking@tripwaalay.com<span>24/7 online support</span></p>
                </li>
            </ul>
        </div>
        <div class="sidenav-social">
            <h3>Follow Us</h3>
            <ul>
                <li><a href="https://www.facebook.com/tripwaalay"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="https://www.instagram.com/tripwaalay/"><i class="fab fa-instagram"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- Sidebar Navigation End -->




    <!--Jquery js-->
    <script src="{{asset('frontend')}}/assets/js/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="{{asset('frontend')}}/assets/js/popper.min.js"></script>
    <!--Bootstrap js-->
    <script src="{{asset('frontend')}}/assets/js/bootstrap.min.js"></script>
    <!--Owl-Carousel js-->
    <script src="{{asset('frontend')}}/assets/js/owl.carousel.min.js"></script>
    <!--Animatedheadline js-->
    <script src="{{asset('frontend')}}/assets/js/jquery.animatedheadline.min.js"></script>
    <!--Slicknav js-->
    <script src="{{asset('frontend')}}/assets/js/jquery.slicknav.min.js"></script>
    <!--Magnific js-->
    <script src="{{asset('frontend')}}/assets/js/jquery.magnific-popup.min.js"></script>
    <!-- Datepicker -->
    <script src="{{asset('frontend')}}/assets/js/jquery.datepicker.min.js"></script>
    <!--Nice Select js-->
    <script src="{{asset('frontend')}}/assets/js/jquery.nice-select.min.js"></script>
    <!-- Way Points JS -->
    <script src="{{asset('frontend')}}/assets/js/waypoints-min.js"></script>
    <!--Main js-->
    <script src="{{asset('frontend')}}/assets/js/main.js"></script>
    <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>
 
    <script>
        var navItems = document.querySelectorAll(".mobile-bottom-nav__item");
        navItems.forEach(function (e, i) {
            e.addEventListener("click", function (e) {
                navItems.forEach(function (e2, i2) {
                    e2.classList.remove("mobile-bottom-nav__item--active");
                })
                this.classList.add("mobile-bottom-nav__item--active");
            });
        });
    </script>

    <script>
        $(":input").inputmask();
        $(function (){
        sel =$("a").filter(function(index) {
            return $(this).attr('href') === window.location.href;
        });
        sel.parents('.mobile-bottom-nav__item').addClass('mobile-bottom-nav__item--active');
        sel.parents('li').addClass('active');
    });
    </script>

<script>
    //Get the button
    var mybutton = document.getElementById("sidenav-toggle1");
    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function () {
       scrollFunction()
    };
    function scrollFunction() {
       if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
          mybutton.style.display = "block";
       } else {
          mybutton.style.display = "none";
       }
    }
 </script>
  <script>
    $(document).on("click", '[data-toggle="lightbox"]', function (event) {
       event.preventDefault();
       $(this).ekkoLightbox();
    });
 </script>

</body>

</html>