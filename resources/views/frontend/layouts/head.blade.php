<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Travel Agency">
    <meta name="keyword" content="travel, tourism, agency, tourist">
    <meta name="author" content="">
    <!-- Title -->
    <title>Tripvaly.com</title>
    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('frontend')}}/assets/img/favicon/favicon-32x32.png">
    <!--Bootstrap css-->
    <link rel="stylesheet" href="{{asset('frontend')}}/assets/css/bootstrap.css">
    <!--Font Awesome css-->
    <link rel="stylesheet" href="{{asset('frontend')}}/assets/css/font-awesome.min.css">
    <!--Flaticon css-->
    <link rel="stylesheet" href="{{asset('frontend')}}/assets/flaticon/flaticon.css">
    <!--Animatedheadline css-->
    <link rel="stylesheet" href="{{asset('frontend')}}/assets/css/jquery.animatedheadline.css">
    <!--Magnific css-->
    <link rel="stylesheet" href="{{asset('frontend')}}/assets/css/magnific-popup.css">
    <!--Owl-Carousel css-->
    <link rel="stylesheet" href="{{asset('frontend')}}/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{asset('frontend')}}/assets/css/owl.theme.default.min.css">
    <!--Animate css-->
    <link rel="stylesheet" href="{{asset('frontend')}}/assets/css/animate.min.css">
    <!--Datepicker css-->
    <link rel="stylesheet" href="{{asset('frontend')}}/assets/css/jquery.datepicker.css">
    <!--Nice Select css-->
    <link rel="stylesheet" href="{{asset('frontend')}}/assets/css/nice-select.css">
    <!--Slicknav css-->
    <link rel="stylesheet" href="{{asset('frontend')}}/assets/css/slicknav.min.css">
    <!--Site Main Style css-->
    <link rel="stylesheet" href="{{asset('frontend')}}/assets/css/style.css">
    <!--Responsive css-->
    <link rel="stylesheet" href="{{asset('frontend')}}/assets/css/responsive.css">
    <!-- Material Icons -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
          integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">


</head>

<body>

