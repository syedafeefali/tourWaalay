@extends('main')
@section('content')

    <!-- About Page Start -->
    <section class="peulis-about-page section_70">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="about-page-left">
                        <h3>About Us</h3>
                        <h2>Who We Are</h2>
                        <p>Rorem Ipsum is simply dummy text of the printin and type setting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown</p>
                        <p>Printer took a galley of type and scrambled it to make a type speci menu book. It has
                            survived not only five centuries, but also the leap intoelectronic typesetting, remaining
                            essentially unchanget was popularised.</p>
                        <div class="about-signature">
                            <div class="signature-left">
                                <img src="{{asset('frontend')}}/assets/img/signature.png" alt="signature">
                            </div>
                            <div class="signature-right">
                                <h3>Robertho Garcia</h3>
                                <p>Founder</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="about-page-right">
                        <img src="{{asset('frontend')}}/assets/img/about.jpg" alt="about page" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About Page End -->


    <!-- Story Area Start -->
    <section class="peulis-story-area">
        <div class="story-area-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="story-box">
                            <h2>Our Story</h2>
                            <p>Quisque massa porta ut placerat lentesque non diam. Nam convallis porta rhoncus. Maecenas
                                varius eget turpis suscipit porta sapien tinc Mauris tempor libero fringilla orci vivrra
                                faucibue fringilla orci vivrra faucibus.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="story-area-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="story-box">
                            <h2>Our Values</h2>
                            <p>Quisque massa porta ut placerat lentesque non diam. Nam convallis porta rhoncus. Maecenas
                                varius eget turpis suscipit porta sapien tinc Mauris tempor libero fringilla orci vivrra
                                faucibue fringilla orci vivrra faucibus.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Story Area End -->


    <!-- Destination Area Start -->
    <section class="peulis-destination-area about_page section_70">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="site-heading">
                        <h2>Destinations</h2>
                        <p>Lorem ipsum dolor sit amet, conseetuer adipiscing elit. Aenan comdo igula eget. Aenean massa
                            cum sociis Theme natoque.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="destination-slider owl-carousel">
                        <div class="single-destination">
                            <div class="destination-image">
                                <a href="#">
                                    <img src="{{asset('frontend')}}/assets/img/destination-1.jpg" alt="destination" />
                                </a>
                                <div class="destination-title">
                                    <h3>Singapore</h3>
                                </div>
                            </div>
                        </div>
                        <div class="single-destination">
                            <div class="destination-image">
                                <a href="#">
                                    <img src="{{asset('frontend')}}/assets/img/destination-2.jpg" alt="destination" />
                                </a>
                                <div class="destination-title">
                                    <h3>Florida</h3>
                                </div>
                            </div>
                        </div>
                        <div class="single-destination">
                            <div class="destination-image">
                                <a href="#">
                                    <img src="{{asset('frontend')}}/assets/img/destination-3.jpg" alt="destination" />
                                </a>
                                <div class="destination-title">
                                    <h3>Portugal</h3>
                                </div>
                            </div>
                        </div>
                        <div class="single-destination">
                            <div class="destination-image">
                                <a href="#">
                                    <img src="{{asset('frontend')}}/assets/img/destination-4.jpg" alt="destination" />
                                </a>
                                <div class="destination-title">
                                    <h3>France</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Destination Area End -->

@endsection