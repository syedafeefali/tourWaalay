@extends('main')
@section('content')
   <!-- Tour Page Start -->
   <section class="peulis-tour-page-area section_70">
    <div class="container">
       <div class="row">
          <div class="col-lg-12" style="margin: 30px 0px;">
             <div class="banner-welcome">
                <form>
                   <p>
                      <i class="fa fa-calendar"></i>
                      <select class="wide">
                         <option>February</option>
                         <option>March</option>
                         <option>April</option>
                         <option>May</option>
                         <option>June</option>
                         <option>July</option>
                         <option>August</option>
                         <option>September</option>
                         <option>October</option>
                         <option>November</option>
                         <option>December</option>
                      </select>
                   </p>

                   <p>
                      <button type="submit"><i class="fa fa-map-marker"></i> Find Now</button>
                   </p>
                </form>
             </div>
          </div>
          <div class="col-lg-12">
             <div class="tour-page-right">

               <div class="row">  
                
            @foreach($tours as $tour)
            
                  
                   <div class="col-lg-4">
                     <div class="single-popular-tour">
                        <div class="popular-tour-image">
                           <img src="{{asset('frontend')}}/assets/img/{{ $tour->photo }}" alt="Popular Tours" />
                           <div class="popular-tour-hover">

                           </div>
                        </div>
                        <div class="popular-tour-desc">
                           <div class="tour-desc-top">
                              <h3><a href="{{ route('tourDetail',$tour->id) }}">{{ $tour->title }}</a></h3>
                              <div class="tour_duration">
                                 <p>
                                    <i class="fa fa-hourglass-half"></i>
                                    {{ $tour->days }} days 
                                 </p>
                              </div>
                              <div class="tour-desc-heading">

                                 <div class="tour_feature">
                                    <a href="#"><i class="fa fa-plane"></i></a>
                                    <a href="#"><i class="fas fa-building"></i></a>
                                    <a href="#"><i class="fas fa-utensils"></i></a>
                                 </div>
                              </div>
                           </div>
                           <div class="tour-desc-bottom">
                              <div class="tour-details">
                                 <a href="{{ route('tourDetail',$tour->id) }}"><i class="fa fa-flag"></i> Book Now</a>
                              </div>
                              <div class="tour-desc-price">
                                 <p> Rs. {{ $tour->price }} / person</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div> 
                 
            @endforeach
               </div>
                  
                
             </div>
          </div>
       </div>
    </div>
 </section>
 <!-- Tour Page End -->

@endsection