@extends('main')
@section('content')
<style>
   .btntoTop{
      display: none;
   }
</style>

   <!-- Breadcrumb Area Start -->
	
   <section class="peulis-breadcrumb-area" style="background: url('{{ asset('frontend')}}/assets/img/{{ $tour->photo }}') no-repeat fixed 0 0/cover;">
    <div class="breadcrumb-top">
       <div class="container">
          <div class="col-lg-12">
             <div class="breadcrumb-box">
                <h2>{{ $tour->title }}</h2>

             </div>
          </div>
       </div>
    </div>
 </section>
 <!-- Breadcrumb Area End -->

 <!-- Tour Details Area Start -->
 <section class="peulis-tour-details-area section_70">
    <div class="container">
       <div class="row">
          <div class="col-lg-12">
             <div class="tour-details-left">

                <p>{!! $tour->description !!}</p>
                <ul class="tour-offer clearfix">
                   <li><span>Destination </span>{{ $tour->destination }}</li>
                   <li><span>Departure </span>{{ $tour->departure }}</li>
                   <li><span>Departure Time </span>{{ $tour->departureTime }}</li>
                   <li><span>Accommodation </span>All Inclusive</li>
                   <li><span>Price </span>{{ $tour->price }} / Person</li>
                   <li>
                      <span>What´s Included</span>
                      <ul>
                        @foreach(explode('|',$tour->include) as $inc)
                        <li><i class="fa fa-check-circle"></i> {{$inc}}</li>                           
                        @endforeach 

                      </ul>
                   </li>
                   <li>
                      <span>Not Included</span>
                      <ul>
                        @foreach(explode('|',$tour->exclude) as $exc)
                         <li><i class="fa fa-times-circle"></i> {{ $exc }} </li>
                        @endforeach
                        </ul>
                   </li>
                </ul>


             </div>
          </div>
          <div class="col-lg-12">
             <div class="sidebar-widget">
                <div class="single-sidebar">

                </div>
                <div class="single-sidebar">
                   <h3>More Information</h3>
                   <ul class="more-info">
                      <li>
                         <span><i class="fa fa-phone"></i> Phone Number:</span>
                         <a href="tel:0306-2202059">0306-2202059</a>
                      </li>
                      <li>
                         <span><i class="fa fa-clock-o"></i> Office Time:</span>
                         9am - 5pm
                      </li>
                      <li>
                         <span><i class="fa fa-map-marker"></i> Office Location:</span>
                         Lahore, Pakistan
                      </li>
                   </ul>
                </div>

             </div>
          </div>
       </div>
    </div>
 </section>
 <!-- Tour Details Area End -->

 <!-- Gallery Area Start -->
 <section class="peulis-gallery-area pt-0 pb-5">
    <div class="container">
       <div class="row">
          <div class="col-lg-12">
             <div class="site-heading">
                <h2>Best Moments</h2>
                <p>Lorem ipsum dolor sit amet, conseetuer adipiscing elit. Aenan comdo igula eget. Aenean massa cum
                   sociis Theme natoque.</p>
             </div>
          </div>
       </div>
       <div class="row">
          <a href="{{asset('frontend')}}/assets/img/gallery-3.jpg" data-toggle="lightbox" data-gallery="gallery" class="col-md-4">
             <img src="{{asset('frontend')}}/assets/img/gallery-3.jpg" class="img-fluid rounded">
          </a>
          <a href="{{asset('frontend')}}/assets/img/gallery-2.jpg" data-toggle="lightbox" data-gallery="gallery" class="col-md-4">
             <img src="{{asset('frontend')}}/assets/img/gallery-2.jpg" class="img-fluid rounded">
          </a>
          <a href="{{asset('frontend')}}/assets/img/gallery-4.jpg" data-toggle="lightbox" data-gallery="gallery" class="col-md-4">
             <img src="{{asset('frontend')}}/assets/img/gallery-4.jpg" class="img-fluid rounded">
          </a>
       </div>
       <div class="row">
          <a href="{{asset('frontend')}}/assets/img/gallery-5.jpg" data-toggle="lightbox" data-gallery="gallery" class="col-md-4">
             <img src="{{asset('frontend')}}/assets/img/gallery-5.jpg" class="img-fluid rounded">
          </a>
          <a href="{{asset('frontend')}}/assets/img/gallery-2.jpg" data-toggle="lightbox" data-gallery="gallery" class="col-md-4">
             <img src="{{asset('frontend')}}/assets/img/gallery-2.jpg" class="img-fluid rounded">
          </a>
          <a href="{{asset('frontend')}}/assets/img/gallery-3.jpg" data-toggle="lightbox" data-gallery="gallery" class="col-md-4">
             <img src="{{asset('frontend')}}/assets/img/gallery-3.jpg" class="img-fluid rounded">
          </a>
       </div>

       <div class="row">
          <div class="col-lg-12">
             <div class="pagination-box-row">
                <ul class="pagination">
                   <li class="active"><a href="#">1</a></li>
                   <li><a href="#">2</a></li>
                   <li><a href="#">3</a></li>
                   <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                </ul>
             </div>
          </div>
       </div>
    </div>
 </section>
 <!-- Gallery Area End -->
 
{{-- //    BOOKING --}}


   <!-- Sidebar Booking Form -->
   <a class="book-now1" id="sidenav-toggle1"><i class="fa fa-flag"></i> Book Now</a>

   <!-- Sidebar Booking Form -->
   <div class="ct-sidenav1">
      <button class="close"><i class="fa fa-times"></i></button>
      <img src="{{asset('frontend')}}/assets/img/logo.png" alt="Site Logo" />
      <div class="sidenav-info">
         <div class="contact-left">

            <form method="POST" action="{{route('clientAdd')}}">
               @csrf
               <input type="hidden" value="{{ $tour->id }}" name="tour_id">
               <h3 class="text-center mb-2">Book Your Tour</h3>

               <div class="input_fields_wrap">
                  <div class="row">
                     <div class="col-lg-6">
                        <p>
                           <input type="text" placeholder="Full Name *" name="name[]" required />
                        </p>
                     </div>
                     <div class="col-lg-6">
                        <p>
                           <input type="tel" placeholder="Phone Number *" data-inputmask="'mask': '0399-99999999'"
                              required="" type="number" name="phone[]" maxlength="12" />
                        </p>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-lg-6">
                        <p>
                           <input type="email" placeholder="Your Email" name="email[]" />
                        </p>
                     </div>
                     <div class="col-lg-6">
                        <p>
                           <input type="text" placeholder="CNIC" data-inputmask="'mask': '99999-9999999-9'"
                              placeholder="XXXXX-XXXXXXX-X" name="cnic[]" />
                        </p>
                     </div>
                  </div>
                  <div class="row">
                  <div class="col text-center">
                     <button class="add_field_button1" data-toggle="tooltip" data-placement="top" title="Add More Person"><i class="fas fa-plus"></i></button>
                     <p class="mt-0 mb-3">Add More Person</p>
                  </div>
               </div>
               </div>
               
               <div class="row">
                  <div class="col-lg-12">
                     <p>
                        <button type="submit">Book Now</button>
                     </p>
                  </div>
               </div>
            </form>
         </div>

      </div>
   </div>
   <!-- Sidebar Booking Form -->

@endsection