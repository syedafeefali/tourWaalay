@extends('main')
@section('content')


    <!-- Contact Area Start -->
    <section class="peulis-contact-area section_70">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="contact-left">
                        <h3>Send Us a Message</h3>
                        <form>
                            <div class="row">
                                <div class="col-lg-6">
                                    <p>
                                        <input type="text" placeholder="Full Name" required />
                                    </p>
                                </div>
                                <div class="col-lg-6">
                                    <p>
                                        <input type="tel" placeholder="Phone Number"
                                            data-inputmask="'mask': '0399-99999999'" required="" type="number"
                                            maxlength="12" />
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>
                                        <input type="email" placeholder="Your Email" />
                                    </p>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>
                                        <textarea placeholder="Your Message" required></textarea>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>
                                        <button type="submit">Send Message</button>
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="contact-right">
                        <h3>Contact Details</h3>
                        <div class="contact-info-item">
                            <div class="contact-info-icon">
                                <i class="fa fa-home"></i>
                            </div>
                            <div class="contact-info-desc">
                                <span>Phone:</span>
                                <ul>
                                    <li>
                                        <a href="tel:0306-2202059">0306-2202059</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="contact-info-item">
                            <div class="contact-info-icon">
                                <i class="fa fa-envelope"></i>
                            </div>
                            <div class="contact-info-desc">
                                <span>Email:</span>
                                <ul>
                                    <li>
                                        <a href="mailto:booking@tripwaalay.com">booking@tripwaalay.com</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="contact-info-item">
                            <div class="contact-info-icon">
                                <i class="fa fa-map-marker"></i>
                            </div>
                            <div class="contact-info-desc">
                                <span>Address:</span>
                                <ul>
                                    <li>Lahore, Pakistan</li>
                                    <li>Street # 1, Super Town</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact Area End -->

@endsection