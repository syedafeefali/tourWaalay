@extends('app')
@section('content')
         <!-- Page Wrapper -->
         <div class="page-wrapper">

            <!-- Page Content -->
            <div class="content container-fluid">

                <!-- Page Header -->
                <div class="page-header">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item active">Customer </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /Page Header -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <div class="card mb-0">
                            <div class="card-header">
                                <h5 class="card-title mb-0">Edit Customer</h5>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm">
                        <form method="POST" action="{{ route('admin.memberUpdate') }}">
                                    @csrf
                                    <input type="hidden" value="{{ $client->id }}" name="id">
                                        <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Full Name</label>
                                                <input class="form-control" type="text" name="name" value="{{ $client->name }}">
                                            </div>
                                        </div>
                                         <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>CNIC</label>
                                                <input class="form-control" name="cnic" value="{{ $client->cnic }}" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    
                                   
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Mobile Number</label>
                                                <input placeholder="" name="phone" value="{{ $client->phone }}" class="form-control" type="tel">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input placeholder="" name="email" value="{{ $client->email }}" class="form-control" type="email">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                       
                                       <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Payment</label>
                                                <input class="form-control" type="text" name="payment" value="{{ $client->payment }}">
                                            </div>
                                       </div>
                                    </div>
                                   
                                    <div class="submit-section">
                                        <button class="btn btn-primary submit-btn">Submit</button>
                                    </div>
                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /Row -->
            </div>
            <!-- /Page Content -->

        </div>
        <!-- /Page Wrapper -->
@endsection