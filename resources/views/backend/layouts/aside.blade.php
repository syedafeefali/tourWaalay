<!-- Sidebar -->
<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                <li>
                    <a href="index.html" class="logo d-flex justify-content-center">
                        <img src="{{asset('backend')}}/assets/img/logo.png" width="100" height="auto" alt="">
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.dashboard')}}"><i class="fas fa-tachometer-alt" style="font-size: 18px;"></i> <span> Dashboard</span></a>
                </li>

                <li class="submenu">
                    <a href="#"><i class="fas fa-mountain fa-sm" style="font-size: 18px;"></i> <span> Tours </span> <span
                                class="menu-arrow"></span></a>
                    <ul style="display: none;">
                        <li><a href="{{route('admin.tourCreate')}}"> Create </a></li>
                        <li><a href="{{route('admin.tourView')}}"> View </a></li>
                    </ul>
                </li>
                <li class="submenu">
                    <a href="#"><i class="fas fa-user-friends" style="font-size: 18px;"></i> <span> Memebers </span> <span
                                class="menu-arrow"></span></a>
                    <ul style="display: none;">
                        <li><a href="{{route('admin.memberView')}}">View </a></li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>
</div>
<!-- /Sidebar -->
