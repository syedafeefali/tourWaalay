
</div>
<!-- /Main Wrapper -->

<!-- jQuery -->
<script src="{{asset('backend')}}/assets/js/jquery-3.2.1.min.js"></script>

<!-- Bootstrap Core JS -->
<script src="{{asset('backend')}}/assets/js/popper.min.js"></script>
<script src="{{asset('backend')}}/assets/js/bootstrap.min.js"></script>

<!-- Slimscroll JS -->
<script src="{{asset('backend')}}/assets/js/jquery.slimscroll.min.js"></script>

<!-- Select2 JS -->
<script src="{{asset('backend')}}/assets/js/select2.min.js"></script>

<!-- Datetimepicker JS -->
<script src="{{asset('backend')}}/assets/js/moment.min.js"></script>
<script src="{{asset('backend')}}/assets/js/bootstrap-datetimepicker.min.js"></script>

<!-- Form Validation JS -->
<script src="{{asset('backend')}}/assets/js/form-validation.js"></script>

<!-- Custom JS -->
<script src="{{asset('backend')}}/assets/js/app.js"></script>

<!-- Datatable JS -->
<script src="{{asset('backend')}}/assets/js/jquery.dataTables.min.js"></script>
<script src="{{asset('backend')}}/assets/js/dataTables.bootstrap4.min.js"></script>

{{--  <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>  --}}
<!-- Summernote JS -->
<script src="{{asset('backend')}}/assets/plugins/summernote/dist/summernote-bs4.min.js"></script>

<!-- Select2 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

<script>
    $('.js-example-basic-single').select2();
</script>

  <!-- Select2 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
  <script src="{{asset('backend')}}/assets/js/multiselect.js"></script>
  <script src="{{asset('backend')}}/assets/js/myjs.js"></script>


  <script>
      $('.js-example-basic-single').select2();
  </script>
  <script>
      $(document).ready(function () {

          $("#file-upload-demo").fileinput({
              'theme': 'explorer',
              'uploadUrl': '#',
              overwriteInitial: false,
              initialPreviewAsData: true,
              initialPreview: [
                  "{{asset('backend')}}/assets/img/profiles/avatar-02.jpg",

              ],
              initialPreviewConfig: [{
                      caption: "{{asset('backend')}}/assets/img/img-01.jpg",
                      size: 329892,
                      width: "100",
                      url: "{{ route('admin.tourSave') }}",
                      key: 1,
                    //   data:{'_token':'{{ csrf_token() }}' }
                  },

              ]
          });

      });
  </script>


</body>

</html>