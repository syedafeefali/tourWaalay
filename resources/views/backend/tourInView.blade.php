@extends('app')
@section('content')
           <!-- Page Wrapper -->
           <div class="page-wrapper">

            <!-- Page Content -->
            <div class="content container-fluid">

                <!-- Page Header -->
                <div class="page-header">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="page-title">Customers</h3>

                        </div>
                        <div class="col-auto float-right ml-auto mb-4">
                            <a href="#" class="btn add-btn" data-toggle="modal" data-target="#create_project"><i
                                    class="fa fa-plus"></i> Create Customer</a>
                            <div class="view-icons">
                            </div>
                        </div>
                    </div>
                    <!-- /Page Header -->

                    <!-- Search Filter -->
                    {{--  <div class="row filter-row">
                        <div class="col-sm-6 col-md-4">
                            <div class="form-group form-focus">
                                <input type="text" class="form-control floating">
                                <label class="focus-label">Tour Title</label>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-4">
                            <div class="form-group form-focus select-focus">
                                <select class="select floating">
                                    <option>Half Payment</option>
                                    <option>Full Payment</option>
                                </select>
                                <label class="focus-label">Payment</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <a href="#" class="btn btn-success btn-block"> Search </a>
                        </div>
                    </div>  --}}
                    <!-- Search Filter -->

                    <div class="row">

                    @foreach($data->persons as $client)
                        <div class="col-md-4 d-flex">
                            <div class="card profile-box flex-fill">
                                <div class="card-body">
                                    <h3 class="card-title">Customer Information
                                        <a href="{{ route('admin.memberEdit',$client->id) }}" target="_blank" class="edit-icon"><i class="fa fa-pencil"></i></a>
                                    </h3>
                                    <ul class="personal-info">
                                        <li>
                                            <div class="title">Full Name.</div>
                                            <div class="text" >{{ $client->name }}</div>
                                        </li>
                                        <li>
                                            <div class="title">CNIC.</div>
                                            <div class="text" >{{ $client->cnic }}</div>
                                        </li>
                                        <li>
                                            <div class="title">Tel</div>
                                            <div class="text" ><a href="">{{ $client->phone }}</a></div>
                                        </li>
                                        <li>
                                            <div class="title">Email</div>
                                            <div class="text" >{{ $client->email }}</div>
                                        </li>
                                        
                                        <li>
                                            <div class="title">Payment Status</div>
                                            <div class="text" >RS {{ $client->payment }}</div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                         </div>
                        @endforeach
                        
                    </div>

                </div>
                <!-- /Page Content -->
                <!-- Create Project Modal -->
                <div id="create_project" class="modal custom-modal fade" role="dialog">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Create Customer</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="{{ route('admin.memberSave') }}">
                                    @csrf
                                    <input type="hidden" value="{{ $data->tour->id }}" name="tour_id">
                                    <input type="hidden" value="{{ $data->id }}" name="parent_id">
                                        <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Full Name</label>
                                                <input class="form-control" type="text" name="name" value="">
                                            </div>
                                        </div>
                                         <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>CNIC</label>
                                                <input class="form-control" name="cnic" value="" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    
                                   
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Mobile Number</label>
                                                <input placeholder="" name="phone" value="" class="form-control" type="tel">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input placeholder="" name="email" value="" class="form-control" type="email">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                       
                                       <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Payment</label>
                                                <input class="form-control" type="text" name="payment" value="">
                                            </div>
                                       </div>
                                    </div>
                                   
                                    <div class="submit-section">
                                        <button class="btn btn-primary submit-btn">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Create Project Modal -->
            </div>
            <!-- /Page Wrapper -->

        </div>
        <!-- /Main Wrapper -->

@endsection