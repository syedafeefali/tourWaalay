@extends('app')
@section('content')
    <!-- Page Wrapper -->
    <div class="page-wrapper">

        <!-- Page Content -->
        <div class="content container-fluid">

            <!-- Page Header -->
            <div class="page-header">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item active">Tours </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /Page Header -->

            <!-- Row -->
            <div class="row">
                <div class="col-sm-12 mb-3">
                    <div class="card mb-0">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Add Tour</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm">
                                    <form class="" method="POST" action="{{ route('admin.tourSave') }}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <label for="title">Title</label>
                                                <input type="text" class="form-control" id="title" name="title"
                                                    value="" required>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="title">Days</label>
                                                <input type="text" class="form-control" id="days" name="days">
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="start-date">Departure</label>
                                                <input type="text" class="form-control" id="departure"
                                                    name="departure" value="" required>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="end-date">Destination</label>
                                                <input type="text" class="form-control" id="destination"
                                                    name="destination" value="" required>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label class="col-form-label">Status</label>
                                                <select class="select" name="status">
                                                    <option value="1">Open</option>
                                                    <option value="0">Close</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="price" class="col-form-label">Price</label>
                                                <input type="text" class="form-control" id="price" name="price">
                                            </div>

                                            {{-- <div class="col-md-6 mb-3">
                                                <label for="persons">Persons</label>
                                                <input type="text" class="form-control" id="persons" name="persons">
                                            </div> --}}
                                            <div class="col-md-6 mb-3">
                                                <label for="account_holder_name">Image</label>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input"
                                                        accept="image/x-png,image/jpeg" id="photo" name="photo">
                                                    <label class="custom-file-label" for="photo">Choose
                                                        file...</label>
                                                </div>
                                            </div>

                                            <div class="col-md-6 mb-3">
                                                <label for="persons">Include</label>
                                                <select name="include[]" multiple data-placeholder="Add tools">
                                                    <option>Travel Insurance</option>
                                                    <option>5 Star Accommodation</option>
                                                    <option>Airport Transfer</option>
                                                    <option>Breakfast</option>
                                                    <option>Personal Guide</option>
                                                    <option>Two days long City tour</option>
                                                </select>
                                                <!-- dribbble -->
                                                <a class="dribbble"
                                                    href="https://dribbble.com/shots/5112850-Multiple-select-animation-field"
                                                    target="_blank"><img
                                                        src="https://cdn.dribbble.com/assets/dribbble-ball-1col-dnld-e29e0436f93d2f9c430fde5f3da66f94493fdca66351408ab0f96e2ec518ab17.png"
                                                        alt=""></a>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="persons">Exclude</label>
                                                <select name="exclude[]" multiple data-placeholder="Add tools">
                                                    <option>Gallery Ticket</option>
                                                    <option>Non-stop flight to Amsterdam</option>

                                                </select>
                                                <!-- dribbble -->
                                                <a class="dribbble"
                                                    href="https://dribbble.com/shots/5112850-Multiple-select-animation-field"
                                                    target="_blank"><img
                                                        src="https://cdn.dribbble.com/assets/dribbble-ball-1col-dnld-e29e0436f93d2f9c430fde5f3da66f94493fdca66351408ab0f96e2ec518ab17.png"
                                                        alt=""></a>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="start-date">Departure Time</label>
                                                <input type="text" class="form-control"
                                                    id="departure-time" name="departureTime" value="" required>
                                            </div>
                                            <div class="col-md-12 mb-3 " id="example1"> 
                                                <label for="end-date">Description</label>
                                                <textarea rows="4" class="form-control summernote"
                                                    placeholder="Enter your message here" name="description"></textarea>
                                            </div>
                                            {{-- <div class="col-md-12 mb-3">
                                                <div class="page-header">
                                                    <h1>File upload demo
                                                    </h1>
                                                </div>
                                                <input id="file-upload-demo" type="file" name="gallery[]" multiple><br />

                                            </div> --}}
                                        </div>
                                        <div class="submit-section mt-0">
                                            <button class="btn btn-primary submit-btn" type="submit">Create</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /Row -->
        </div>
        <!-- /Page Content -->

    </div>
    <!-- /Page Wrapper -->
@endsection