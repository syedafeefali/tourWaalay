@extends('app')
@section('content')
        <!-- Page Wrapper -->
        <div class="page-wrapper">

            <!-- Page Content -->
            <div class="content container-fluid">

                <!-- Page Header -->
                <div class="page-header">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item active">View Customers</li>
                            </ul>
                        </div>
                        <div class="col-auto float-right ml-auto mb-4">
                            <a href="#" class="btn add-btn" data-toggle="modal" data-target="#create_project"><i
                                    class="fa fa-plus"></i> Create Customer</a>
                            <div class="view-icons">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Page Header -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-striped custom-table mb-0 datatable">
                                <thead>
                                    <tr>
                                        <th>Customer Name </th>
                                        <th>Phone Number</th>
                                        <th>Payment Status</th>
                                        <th>Tour Name</th>
                                        {{-- <th>Status</th> --}}
                                        <th>No of Persons</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                             @foreach($clients as $client)
                                    <tr>
                                        <td>{{ $client->name }}</td>
                                        <td>{{ $client->phone }}</td>
                                        <td>{{ $client->payment }}</td>
                                        <td>{{ $client->tour->title }}</td>
                                        {{-- <td>
                                            <span class="badge bg-inverse-danger">Half Payment</span>
                                        </td> --}}
                                        <td>
                                           {{count($client->persons()->get())}}
                                        </td>
                                       
                                        <td>
                                            <a href="{{ route('admin.memberEdit',$client->id) }}" data-toggle="tooltip" data-placement="top"
                                                title="Edit" class="bell-icon" data-original-title="Edit">
                                                <i class="fas fa-pencil fa-lg"></i>
                                            </a>
                                            <a href="{{ route('admin.tourInView',$client->id) }}" data-toggle="tooltip" data-placement="top" title="View"
                                                class="bell-icon ml-1" data-original-title="View">
                                                <i class="fa fa-eye fa-lg"></i>
                                            </a>

                                        </td>
                                    </tr>  
                             @endforeach
                                    
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /Page Content -->

            <div id="create_project" class="modal custom-modal fade" role="dialog">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Create Customer</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="{{ route('admin.memberSave') }}">
                                @csrf
                                {{--  <input type="hidden" value="{{ $id->id }}" name="tour_id">  --}}
                                    <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Full Name</label>
                                            <input class="form-control" type="text" name="name" value="">
                                        </div>
                                    </div>
                                     <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>CNIC</label>
                                            <input class="form-control" name="cnic" value="" type="text">
                                        </div>
                                    </div>
                                </div>
                                
                               
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Mobile Number</label>
                                            <input placeholder="" name="phone" value="" class="form-control" type="tel">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input placeholder="" name="email" value="" class="form-control" type="email">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                   
                                   <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Payment</label>
                                            <input class="form-control" type="text" name="payment" value="">
                                        </div>
                                   </div>
                                   <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Select Tour</label>
                                            <select name="tour_id" id="">
                                                @foreach($id as $tour)
                                                <option value="{{ $tour->id }}">{{ $tour->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                   </div>
                                </div>
                               
                                <div class="submit-section">
                                    <button class="btn btn-primary submit-btn">Submit</button>
                                </div>
                    </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /Page Wrapper -->
@endsection