@extends('app')
@section('content')
    <!-- Page Wrapper -->
    <div class="page-wrapper">

        <!-- Page Content -->
        <div class="content container-fluid">

            <!-- Page Header -->
            <div class="page-header">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item active">View Tours</li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /Page Header -->

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped custom-table mb-0 datatable">
                            <thead>
                            <tr>
                                <th>Sr#</th>
                                <th>Title </th>
                                <th>Days</th>
                                <th>Destination</th>
                                <th>Departure Time</th>
                                <th>Total Persons</th>
                                <th>Image</th>
                                <th>Price</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                        @foreach($records as $data)
                            
                            <tr>
                                <td>{{ $loop->index+1 }}</td>
                                <td>{{ $data->title }}</td>
                                <td>{{ $data->days }}</td>
                                <td>{{ $data->destination }}</td>
                                <td>{{ $data->departureTime }}</td>
                                <td>{{ count($data->clients()->get()) }}</td>
                                {{-- <td>
                                    <span class="badge bg-inverse-danger">Close</span>
                                </td>--}}
                                <td>
                                    <img src="{{ asset('frontend/') }}/assets/img/{{ $data->photo }}" style="width: 80px" alt="">
                                </td> 
                                <td>RS {{ $data->price }}</td>
                                <td>
                                    <a href="{{route('admin.tourEdit',$data->id)}}" data-toggle="tooltip" data-placement="top"
                                       title="Edit" class="bell-icon" data-original-title="Edit">
                                        <i class="fas fa-pencil fa-lg"></i>
                                    </a>
                                    <a href="{{route('admin.tourClients',$data->id)}}" data-toggle="tooltip" data-placement="top" title="View"
                                       class="bell-icon ml-1" data-original-title="View">
                                        <i class="fa fa-eye fa-lg"></i>
                                    </a>

                                    <a href="{{route('admin.tourClone',$data->id)}}" data-toggle="tooltip" data-placement="top" title="Clone"
                                       class="bell-icon ml-1" data-original-title="Clone">
                                        <i class="far fa-clone fa-lg"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <!-- /Page Content -->

    </div>
    <!-- /Page Wrapper -->
@endsection