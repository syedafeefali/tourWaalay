@extends('app')
@section('content')
    <!-- Page Wrapper -->
    <div class="page-wrapper">

        <!-- Page Content -->
        <div class="content container-fluid">

            <!-- Page Header -->
            <div class="page-header">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item active">Tours </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /Page Header -->

            <!-- Row -->
            <div class="row">
                <div class="col-sm-12 mb-3">
                    <div class="card mb-0">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Edit Tour</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm">
                                    <form class="" method="POST" action="{{ route('admin.tourSave') }}">
                                        @csrf
                                        <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <label for="title">Title</label>
                                                <input type="text" class="form-control" id="title" name="title"
                                                       value="{{ $data->title }}" required>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="title">Days</label>
                                                <input type="text" class="form-control" id="days" value="{{ $data->days }}" name="days">
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="start-date">Start Date</label>
                                                <input type="text" class="form-control datetimepicker"
                                                       id="start-date" name="startDate" value="{{ $data->startDate }}" required>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="end-date">End Date</label>
                                                <input type="text" class="form-control datetimepicker" id="end-date"
                                                       name="endDate" value="{{ $data->endDate }}" required>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label class="col-form-label">Status</label>
                                                <select class="select" name="status">
                                                    <option value="1">Open</option>
                                                    <option value="0">Close</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="price" class="col-form-label">Price</label>
                                                <input type="text" class="form-control" id="price" name="price">
                                            </div>

                                            <div class="col-md-6 mb-3">
                                                <label for="persons">Persons</label>
                                                <input type="text" class="form-control" id="persons" name="persons">
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="account_holder_name">Image</label>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input"
                                                           accept="image/x-png,image/jpeg" id="photo" name="photo">
                                                    <label class="custom-file-label" for="photo">Choose
                                                        file...</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12 mb-3">
                                                <label for="end-date">Description</label>
                                                <textarea rows="4" class="form-control summernote" name="description"
                                                          placeholder="Enter your message here">{{ $data->description }}</textarea>
                                            </div>
                                        </div>
                                        <div class="submit-section mt-0">
                                            <button class="btn btn-primary submit-btn" type="submit">Create</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /Row -->
        </div>
        <!-- /Page Content -->

    </div>
    <!-- /Page Wrapper -->
@endsection